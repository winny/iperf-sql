import os
from dotenv import load_dotenv
import sys
import psycopg2
import ijson
import logging
from datetime import datetime

logging.basicConfig(level=logging.DEBUG)

INSERT = """
INSERT INTO entry (
  version,
  system_info,
  protocol,
  timestamp,
  entry_type,
  cookie,
  num_streams,
  blksize,
  omit,
  duration,
  bytes,
  blocks,
  reverse,
  tos,
  host_total_cpu,
  host_user_cpu,
  host_system_cpu,
  remote_total_cpu,
  remote_user_cpu,
  remote_system_cpu
) VALUES (
  %(version)s,
  %(system_info)s,
  %(protocol)s,
  %(timestamp)s,
  %(entry_type)s,
  %(cookie)s,
  %(num_streams)s,
  %(blksize)s,
  %(omit)s,
  %(duration)s,
  %(bytes)s,
  %(blocks)s,
  %(reverse)s,
  %(tos)s,
  %(host_total_cpu)s,
  %(host_user_cpu)s,
  %(host_system_cpu)s,
  %(remote_total_cpu)s,
  %(remote_user_cpu)s,
  %(remote_system_cpu)s
);
"""

if __name__ == '__main__':
    load_dotenv()
    logging.debug('Connecting to Postgres...')
    with psycopg2.connect('') as conn:
        logging.debug('Connected.')
        objects = ijson.items(sys.stdin, '', multiple_values=True)
        for obj in objects:
            with conn.cursor() as cur:
                logging.debug('Inserting %s' % obj)
                if 'connecting_to' in obj['start']:
                    entry_type = 'client'
                elif 'accepted_connection' in obj['start']:
                    entry_type = 'server'
                else:
                    raise TypeError('Not a well formed iperf JSON object. %s' % obj)
                cur.execute(INSERT, {
                    'version': obj['start']['version'],
                    'system_info': obj['start']['system_info'],
                    'protocol': obj['start']['test_start']['protocol'].lower(),
                    'timestamp': datetime.utcfromtimestamp(obj['start']['timestamp']['timesecs']),
                    'entry_type': entry_type,
                    'cookie': obj['start']['cookie'],
                    'num_streams': obj['start']['test_start']['num_streams'],
                    'blksize': obj['start']['test_start']['blksize'],
                    'omit': obj['start']['test_start']['omit'],
                    'duration': obj['start']['test_start']['duration'],
                    'bytes': obj['start']['test_start']['bytes'],
                    'blocks': obj['start']['test_start']['blocks'],
                    'reverse': bool(obj['start']['test_start']['reverse']),
                    'tos': obj['start']['test_start']['tos'],
                    'host_total_cpu': obj['end']['cpu_utilization_percent']['host_total'],
                    'host_user_cpu': obj['end']['cpu_utilization_percent']['host_user'],
                    'host_system_cpu': obj['end']['cpu_utilization_percent']['host_system'],
                    'remote_total_cpu': obj['end']['cpu_utilization_percent']['remote_total'],
                    'remote_user_cpu': obj['end']['cpu_utilization_percent']['remote_user'],
                    'remote_system_cpu': obj['end']['cpu_utilization_percent']['remote_system'],
                })

