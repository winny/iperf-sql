CREATE DOMAIN HOSTNAME as VARCHAR(120);

CREATE DOMAIN UTILIZATION as NUMERIC(20, 16)
CHECK(
        VALUE >= 0.0 AND VALUE <= 100.0
);

CREATE DOMAIN PORT as INTEGER
CHECK(
        VALUE >= 0 AND VALUE <= 65535
);

CREATE DOMAIN UINT as INTEGER
CHECK(
        VALUE >= 0
);

CREATE TYPE entry_type AS enum (
       'client',
       'server'
);

CREATE TYPE protocol AS enum (
       'sctp',
       'tcp',
       'udp'
);

CREATE TABLE entry (
       id SERIAL,
       version VARCHAR(120) NOT NULL,
       system_info VARCHAR(120) NOT NULL,
       protocol PROTOCOL NOT NULL,
       timestamp TIMESTAMP NOT NULL,
       entry_type ENTRY_TYPE NOT NULL,
       cookie VARCHAR(37) NOT NULL,
       num_streams UINT NOT NULL,
       blksize UINT NOT NULL,
       omit UINT NOT NULL,
       duration UINT NOT NULL,
       bytes UINT NOT NULL,
       blocks UINT NOT NULL,
       reverse BOOLEAN NOT NULL,
       tos UINT NOT NULL,
       host_total_cpu UTILIZATION NOT NULL,
       host_user_cpu UTILIZATION NOT NULL,
       host_system_cpu UTILIZATION NOT NULL,
       remote_total_cpu UTILIZATION NOT NULL,
       remote_user_cpu UTILIZATION NOT NULL,
       remote_system_cpu UTILIZATION NOT NULL
);
